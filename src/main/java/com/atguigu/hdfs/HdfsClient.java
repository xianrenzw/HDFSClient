package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

public class HdfsClient {

    private FileSystem fs;

    @Before
    public void init() throws URISyntaxException, IOException, InterruptedException {
        // 创建一个配置文件
        Configuration conf = new Configuration();

        // 获取到客户端对象  参数：集群nn地址，配置文件，用户名
        fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
    }

    @After
    public void close() throws IOException {
        // 关闭资源
        fs.close();
    }

    // 创建目录
    @Test
    public void testMkdirs() throws IOException, URISyntaxException, InterruptedException {
        fs.mkdirs(new Path("/input"));
    }

    // 上传文件

    /**
     * 参数优先级排序：(由高到低)
     * （1）客户端代码中设置的值 >
     * （2）项目下用户自定义配置文件 >
     * （3）然后是服务器的自定义配置（xxx-site.xml） >
     * （4）服务器的默认配置（xxx-default.xml）
     *
     * @throws IOException
     */
    @Test
    public void testCopyFromLocalFile() throws IOException {
        // 参数：是否删除原文件  是否允许覆盖  原文件路径  目的地路径
        fs.copyFromLocalFile(false, true,
                new Path("D:\\word.txt"),
                new Path("/input"));
    }

    //文件下载
    @Test
    public void testCopyToLocalFile() throws IOException, InterruptedException, URISyntaxException {
        // 执行下载操作
        // boolean delSrc 指是否将原文件删除
        // Path src 指要下载的文件路径
        // Path dst 指将文件下载到的路径
        // boolean useRawLocalFileSystem 是否开启crc文件校验 false是开启 ture是关闭
        fs.copyToLocalFile(false,
                new Path("/xiyou/huaguoshan/sunwukong.txt"),
                new Path("D:\\sunwukong.txt"),
                true);
    }

    //文件删除
    @Test
    public void testDelete() throws IOException {
        // 执行删除
        //参数： 要删除的路径  是否递归删除
        fs.delete(new Path("/input/"), true);
    }

    //文件的更名和移动
    @Test
    public void testRename() throws IOException {
        //参数：原文件路径  目标文件路径
        fs.rename(new Path("/xiyou/huaguoshan/sunwukong.txt"),
                new Path("/xiyou/huaguoshan/meihouwang.txt"));

    }

    //获取文件详情
    @Test
    public void testListFiles() throws IOException {
        // 获取文件详情
        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("/input"), true);
        //用上一步获取的迭代器遍历文件
        while (listFiles.hasNext()) {
            LocatedFileStatus fileStatus = listFiles.next();

            System.out.println("========" + fileStatus.getPath() + "=========");
            System.out.println(fileStatus.getPermission());
            System.out.println(fileStatus.getOwner());
            System.out.println(fileStatus.getGroup());
            System.out.println(fileStatus.getLen());
            System.out.println(fileStatus.getModificationTime());
            System.out.println(fileStatus.getReplication());
            System.out.println(fileStatus.getBlockSize());
            System.out.println(fileStatus.getPath().getName());

            // 获取块信息
            BlockLocation[] blockLocations = fileStatus.getBlockLocations();
            System.out.println(Arrays.toString(blockLocations));
        }

    }

    //判断是文件还是文件夹
    @Test
    public void testFile() throws IOException {

        FileStatus[] listStatus = fs.listStatus(new Path("/"));

        for (FileStatus Status : listStatus) {

            if (Status.isFile()) {
                System.out.println("文件：" + Status.getPath().getName());
            } else {
                System.out.println("文件夹：" + Status.getPath().getName());
            }
        }

    }


}