package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;

import java.io.IOException;  
import java.net.URI;  
import java.net.URISyntaxException;  
  
public class HDFSDirectoryOperations {  
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {
        String hdfsDirPath = "/input/test";
        Configuration conf = new Configuration();  
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf,"atguigu");
        // 创建目录，如果目录的父目录不存在，则自动创建  
        Path dirPath = new Path(hdfsDirPath);  
        if (!fs.exists(dirPath)) {  
            fs.mkdirs(dirPath, new FsPermission("755"));
            System.out.println("Directory created: " + hdfsDirPath);  
        } else {  
            System.out.println("Directory already exists: " + hdfsDirPath);  
        }
        // 删除目录，询问用户是否要删除非空目录  
        boolean recursiveDelete = false;  
        System.out.print("Delete directory " + hdfsDirPath + " recursively (y/n)? ");  
        char choice = (char) System.in.read();  
        if (choice == 'y' || choice == 'Y') {  
            recursiveDelete = true;  
        }
        boolean isDeleted = fs.delete(dirPath, recursiveDelete);  
        System.out.println("Directory deleted: " + isDeleted);  
  
        fs.close();  
    }  
}