package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import java.net.URI;  
import java.net.URISyntaxException;  
import java.io.IOException;  
  
public class HDFSDeleteDirectory {  
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        String hdfsDirectoryPath = "/input/test/";
        boolean recursive = true; // 是否递归删除目录及其内容

        Configuration conf = new Configuration();  
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
        Path directoryPath = new Path(hdfsDirectoryPath);
        //参数： 要删除的路径  是否递归删除
        boolean isDeleted = fs.delete(directoryPath, recursive);  
        if (isDeleted) {  
            System.out.println("Directory deleted successfully: " + hdfsDirectoryPath);  
        } else {  
            System.out.println("Failed to delete directory: " + hdfsDirectoryPath);  
        }
        fs.close();  
    }  
}