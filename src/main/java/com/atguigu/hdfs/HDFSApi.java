package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class HDFSApi {
    public static void main(String[] args) throws Exception {
        // HDFS文件路径
        String hdfsFilePath = "hdfs://hadoop102:8020/input/word.txt";

        // 设置Hadoop的URL流处理器工厂
        URL.setURLStreamHandlerFactory(new FsUrlStreamHandlerFactory());

        // 创建Hadoop配置对象
        Configuration conf = new Configuration();
        conf.set("atguigu", "hdfs://hadoop102:8020");

        // 使用URL打开HDFS文件
        URL url = new URL(hdfsFilePath);
        URLConnection connection = url.openConnection();
        connection.setDefaultUseCaches(false);
        connection.connect();

        // 读取文件内容
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
}