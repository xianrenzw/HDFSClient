package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import java.net.URI;  
import java.net.URISyntaxException;  
import java.io.IOException;  
  
public class HDFSMoveFile {  
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        String sourceFilePath = "/input/word.txt";
        String destinationFilePath = "/output/word.txt";
  
        Configuration conf = new Configuration();  
        FileSystem fs =  FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
  
        Path sourcePath = new Path(sourceFilePath);  
        Path destinationPath = new Path(destinationFilePath);  
  
        boolean isMoved = fs.rename(sourcePath, destinationPath);  
        if (isMoved) {  
            System.out.println("File moved successfully from " + sourceFilePath + " to " + destinationFilePath);  
        } else {  
            System.out.println("Failed to move file from " + sourceFilePath + " to " + destinationFilePath);  
        }  
  
        fs.close();  
    }  
}