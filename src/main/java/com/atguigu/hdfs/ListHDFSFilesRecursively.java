package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ListHDFSFilesRecursively {
    public static void main(String[] args) throws Exception {
        String hdfsDirPath = "/input"; // HDFS中的目录路径
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
        Path path = new Path(hdfsDirPath);
        listDirectoryRecursively(fs, path, 0);
        fs.close();
    }

    private static void listDirectoryRecursively(FileSystem fs, Path path, int indent) throws IOException {
        FileStatus[] fileStatuses = fs.listStatus(path);
        for (FileStatus fileStatus : fileStatuses) {
            Path filePath = fileStatus.getPath();
            FsPermission permission = fileStatus.getPermission();
            long fileSize = fileStatus.getLen();
            long modificationTime = fileStatus.getModificationTime();
            String fileType = fileStatus.isDirectory() ? "directory" : "file";
            // 打印缩进以表示目录层次  
            for (int i = 0; i < indent; i++) {
                System.out.print("----");
            }
            // 输出文件信息  
            System.out.println("Path: " + filePath);
            System.out.println("Type: " + fileType);
            System.out.println("Permission: " + permission);
            System.out.println("Size: " + fileSize + " bytes");
            System.out.println("Modification Time: " +
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                            .format(new Date(modificationTime)));
            // 如果是目录，则递归输出该目录下的所有文件信息  
            if (fileStatus.isDirectory()) {
                listDirectoryRecursively(fs, filePath, indent + 1);
            }
        }
    }
}