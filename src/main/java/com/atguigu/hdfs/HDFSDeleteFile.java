package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.IOUtils;  
  
import java.net.URI;  
import java.net.URISyntaxException;  
import java.io.IOException;  
  
public class HDFSDeleteFile {  
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        String hdfsFilePath = "/input/word.txt";
        Configuration conf = new Configuration();  
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
  
        Path filePath = new Path(hdfsFilePath);  
        boolean isDeleted = fs.delete(filePath, true); // 第二个参数true表示如果路径是目录则递归删除  
        if (isDeleted) {  
            System.out.println("File deleted successfully: " + hdfsFilePath);  
        } else {  
            System.out.println("Failed to delete file: " + hdfsFilePath);  
        }  
  
        IOUtils.closeStream(fs);  
    }  
}