package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

public class ReadHDFSFileToConsole {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        String hdfsFilePath = "/input/word.txt"; // HDFS中的文件路径
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
        Path hdfsPath = new Path(hdfsFilePath);
        try (FSDataInputStream inputStream = fs.open(hdfsPath)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line); // 输出到终端
            }
        }

    }
}