package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

public class MyFSDataInputStream extends FSDataInputStream {

    public MyFSDataInputStream(InputStream in) {
        super(in);
    }

    /**
     * 按行读取文本
     *
     * @param br BufferedReader对象，用于读取文本
     * @return 读取到的行文本，如果没有读取到任何内容则返回null
     * @throws IOException 如果读取过程中出现I/O错误
     */
    public static String readLine(BufferedReader br) throws IOException {
        StringBuilder sb = new StringBuilder();
        int c;
        while ((c = br.read()) != -1) {
            char ch = (char) c;
            if (ch == '\n') {
                // 如果遇到换行符，则结束读取并返回之前读取的字符
                break;
            } else {
                // 否则，将字符添加到StringBuilder中
                sb.append(ch);
            }
        }
        if (sb.length() > 0) {
            // 如果StringBuilder中有内容，则返回其内容作为字符串
            return sb.toString();
        } else {
            // 如果没有读取到任何内容，返回null
            return null;
        }
    }

    /**
     * 读取文件内容
     */
    public static void cat(Configuration conf, String remoteFilePath) throws IOException, URISyntaxException, InterruptedException {
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
        Path remotePath = new Path(remoteFilePath);
        FSDataInputStream in = fs.open(remotePath);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String line = null;
        // 按行读取
        while ((line = MyFSDataInputStream.readLine(br)) != null) {
            System.out.println(line);
        }
        br.close();
        in.close();
        fs.close();
    }

    public static void main(String[] args) {
        Configuration conf = new Configuration();
        String remoteFilePath = "/input/word.txt"; // HDFS路径
        try {
            MyFSDataInputStream.cat(conf, remoteFilePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}