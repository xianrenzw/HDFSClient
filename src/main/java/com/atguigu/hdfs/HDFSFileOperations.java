package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.permission.FsPermission;

import java.io.IOException;  
import java.net.URI;  
import java.net.URISyntaxException;  
  
public class HDFSFileOperations {  
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {
        String hdfsFilePath = "/input/hello.txt";
        Configuration conf = new Configuration();  
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf,"atguigu");
        // 创建目录（如果目录不存在）  
        Path filePath = new Path(hdfsFilePath);  
        Path parentPath = filePath.getParent();  
        if (parentPath != null && !fs.exists(parentPath)) {  
            fs.mkdirs(parentPath, new FsPermission("755"));
        }  
  
        // 创建文件  
        FSDataOutputStream outputStream = fs.create(filePath);
        outputStream.close();
        // 验证文件是否创建成功  
        System.out.println("File created: " + fs.exists(filePath));
        // 删除文件  
        boolean isDeleted = fs.delete(filePath, false);  
        System.out.println("File deleted: " + isDeleted);  
  
        fs.close();  
    }  
}