package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

public class HDFSFileUpload {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        String localFilePath = "D:\\word.txt"; // 本地文件路径
        String hdfsFilePath = "/input/word.txt"; // HDFS文件路径
        boolean overwrite = true; // 如果为true，则覆盖；如果为false，则追加
        // 1. 初始化Hadoop配置和文件系统对象  
        Configuration conf = new Configuration();  
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
        // 2. 检查HDFS中是否已存在文件  
        if (fs.exists(new Path(hdfsFilePath))) {  
            System.out.println("File already exists in HDFS. Overwrite? [y/n]");  
            char choice = (char) System.in.read();  
            overwrite = choice == 'y' || choice == 'Y';  
        }
        // 3. 根据用户选择，追加或覆盖文件  
        if (overwrite) {  
            // 覆盖文件  
            fs.copyFromLocalFile(new Path(localFilePath),new Path(hdfsFilePath));
            System.out.println("File overwritten in HDFS.");
            fs.close();
        } else {  
            // 追加到文件末尾  
            InputStream in = new BufferedInputStream(new FileInputStream(localFilePath));  
            FSDataOutputStream out = fs.append(new Path(hdfsFilePath));  
            byte[] b = new byte[1024];  
            int numBytes = 0;  
            while ((numBytes = in.read(b)) > 0) {  
                out.write(b, 0, numBytes);  
            }  
            in.close();  
            out.close();  
            fs.close();  
            System.out.println("File appended to HDFS.");  
        }  
    }  
}
