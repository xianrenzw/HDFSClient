package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import java.io.File;  
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
  
public class HdfsDownloadWithRename {  
    public static void main(String[] args) {
        String hdfsFilePath = "/input/word.txt"; // HDFS中的文件路径
        String localDir = "D:\\"; // 本地目录路径
        String localFileName = "word.txt"; // 预期下载的本地文件名
        String newLocalFileName = getNewFileName(localDir, localFileName); // 如果需要，生成新文件名  
        Configuration conf = new Configuration();
        try (FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");) {
            Path hdfsPath = new Path(hdfsFilePath);
            File localFile = new File(localDir, newLocalFileName);
            fs.copyToLocalFile(hdfsPath, new Path(localFile.getAbsolutePath()));
            System.out.println("File downloaded to " + localFile.getAbsolutePath());
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }  
  
    // 检查本地文件是否存在，如果存在则生成新的文件名（例如，添加时间戳）
    private static String getNewFileName(String localDir, String localFileName) {  
        File localFile = new File(localDir, localFileName);  
        if (localFile.exists()) {  
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");  
            String timestamp = sdf.format(new Date());  
            return localFileName + "_" + timestamp;  
        }  
        return localFileName;  
    }  
}