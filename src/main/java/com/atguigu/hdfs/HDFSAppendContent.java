package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;  
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.IOUtils;  
import java.io.BufferedInputStream;  
import java.io.ByteArrayInputStream;  
import java.io.InputStream;  
import java.net.URI;  
import java.net.URISyntaxException;  
import java.io.IOException;  
  
public class HDFSAppendContent {  
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {
        String hdfsFilePath = "/input/word.txt";
        String contentToAdd = "This is the content to append";  
        boolean appendToStart = false; // true 是加到开头, false 加到末尾
  
        Configuration conf = new Configuration();  
        FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
  
        Path filePath = new Path(hdfsFilePath);  
        if (appendToStart) {
            FSDataOutputStream outputStream = fs.create(filePath);  
            outputStream.writeUTF(contentToAdd);  
            InputStream inputStream = fs.open(filePath);  
            IOUtils.copyBytes(inputStream, outputStream, conf);
            IOUtils.closeStream(inputStream);  
            outputStream.close();  
        } else {  
            // Appending to end is straightforward  
            FSDataOutputStream appendStream = fs.append(filePath);  
            appendStream.writeUTF(contentToAdd);  
            appendStream.close();  
        }
        fs.close();  
        System.out.println("文本已添加到文件: " + hdfsFilePath);
    }  
}