package com.atguigu.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.security.UserGroupInformation;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HDFSFileInfo {
    public static void main(String[] args) {
        String hdfsFilePath = "/input/word.txt"; // HDFS中的文件路径
        Configuration conf = new Configuration();
        try {
            FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), conf, "atguigu");
            Path path = new Path(hdfsFilePath);
            // 获取文件状态
            FileStatus fileStatus = fs.getFileStatus(path);
            // 输出文件信息
            System.out.println("Path: " + fileStatus.getPath());
            System.out.println("Length: " + fileStatus.getLen());
            System.out.println("Owner: " + fileStatus.getOwner());
            System.out.println("Group: " + fileStatus.getGroup());
            System.out.println("Permission: " + fileStatus.getPermission());
            // 获取文件的创建时间
            long creationTime = fileStatus.getModificationTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String creationTimeStr = sdf.format(new Date(creationTime));
            System.out.println("Creation Time: " + creationTimeStr);
            // 注意：Hadoop FileSystem API 不直接提供文件的创建时间。
            // 上述代码中的 creationTime 实际上是文件的最后修改时间。
            // 若要获取文件的创建时间，您可能需要使用其他方法，
            // 比如查看文件系统的审计日志或利用Hadoop的底层存储系统的特性。
            fs.close();
        } catch (IOException | URISyntaxException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}